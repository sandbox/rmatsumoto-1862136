
Introduction to Basic

BASIC was originally developed for internal use to develop themes at Raincity Studios (Vancouver)
After using the ZEN theme for years, we realised that it was getting too complicated, had too much
files and overrides, so we decided to develop a strip down version of it and BASIC
was created.

ZEN became a fairly big piece of code and we felt like for each project we didn't need most of
it. So we took what we use all the time in ZEN, and removed all the things we felt were unnecessary.

The layout was also modified to make it easier to modify. Most of the CSS was brought down to a 
strict minimum, and the templates were also recoded to make them as clear as possible.

BASIC is not intended for beginners, and if you're not sure, try ZEN first, and maybe later
try basic.
BASIC is now used for professional projects by multiple drupal agencies around the world.

__________________________________________________________________________________________

Intro to Basic - Responive

Based on Basic, Basic - Responsive, as name suggests, adds responsive css to Basic theme.
This theme is intended for internal use at FCV (http://www.fcv.ca/) Drupal team.

Intent to develop this theme was because of how all responsive theme recently created and submitted
to drupal.org was complex and complicated and because every client has different demand
in terms of design and functionality, it was pain to strip to unnecessary components.

So instead of removing unnecessary components, we wanted to create something that had bare
minimum theme functionality and build components on top, based on clients' demand.

While it is possible to use this theme without SASS, use of Compass and SASS is recommended.

__________________________________________________________________________________________

What's been changed from original Basic Theme

- Sass is now using .scss extension instead of .sass extension
This is for more readability. Drupal have requirements of 2 spaces for indentation and with how
.sass works, it makes code hard to read, thus using scss instead.

- Now using config.rb file instead of relying on compass module

- Sass and CSS files strutures has been change.
While it still uses same stylings, file structures has been change for easier responsive design
CSS and maintenance

__________________________________________________________________________________________

What are the files for ?
------------------------

- basic.info => provide informations about the theme, like regions, css, settings, js ...
- block-system-main.tpl.php => template to edit the content
- block.tpl.php => template to edit the blocks
- comment.tpl.php => template to edit the comments
- node.tpl.php => template to edit the nodes (in content)
- page.tpl.php => template to edit the page
- template.php => used to modify drupal's default behavior before outputting HTML through 
  the theme
- theme-settings => used to create additional settings in the theme settings page

In /CSS
-------

- default.css => define default classes, browser resets and admin styles
- ie7 => used to debug IE7
- layout.css => define the layout of the theme
- print.css => define the way the theme look like when printed
- style.css => contains some default font styles. that's where you can add custom css
- tabs.css => styles for the admin tabs (from ZEN)

__________________________________________________________________________________________

Changing the Layout

The layout used in Basic is fairly similar to the Holy Grail method. It has been tested on 
all major browser including IE (5>8), Opera, Firefox, Safari, Chrome ...
The purpose of this method is to have a minimal markup for an ideal display. 
For accessibility and search engine optimization, the best order to display a page is ]
the following :

	1. header
	2. content
	3. sidebars
	4. footer

This is how the page template is buit in basic, and it works in fluid and fixed layout.
Refers to the notes in layout.css to see how to modify the layout.

__________________________________________________________________________________________

Thanks for using BASIC - Responsive, and remember to use the issue queue in drupal.org for any question
or bug report:

@TODO

Current Basic maintainers:
* Hubert Florin (couzinhub) -http://drupal.org/user/133581
* Steve Krueger (SteveK) -http://drupal.org/user/111656

Basic - Responsive maintainer:
* Ryota Matsumoto (rmatsumoto) - http://drupal.org/user/1138848/