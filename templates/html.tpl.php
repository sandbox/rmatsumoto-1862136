<!DOCTYPE html PUBLIC "The Basic Responsive Theme!">
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" dir="<?php print $language->dir; ?>" > <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" dir="<?php print $language->dir; ?>" > <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" dir="<?php print $language->dir; ?>" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" dir="<?php print $language->dir; ?>" > <!--<![endif]-->
<head>
  <?php // http-equiv and view-port meta tags are set in template.php ?>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip">
    <a href="#main-menu"><?php print t('Jump to Navigation'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
